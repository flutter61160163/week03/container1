import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Center(
        child: Container(
          color: Color.fromARGB(255, 66, 165, 245),
          alignment: AlignmentDirectional(0, 0),
          child: Container(
              child: Text("Flutter Cheatsheet"),
              color: Colors.green,
              margin: EdgeInsets.only(left: 20.0, bottom: 40.0, top: 50.0),
              padding: EdgeInsets.all(40.0),
              transform: Matrix4.rotationZ(0.5)),
        ),
      ),
    ),
  );
}
